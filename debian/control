Source: ruby-batch-loader
Section: ruby
Priority: optional
Maintainer: Debian Ruby Extras Maintainers <pkg-ruby-extras-maintainers@lists.alioth.debian.org>
Uploaders: Pirate Praveen <praveen@debian.org>
Build-Depends: debhelper (>= 11~),
               gem2deb,
               ruby-graphql,
               ruby-rspec
Standards-Version: 4.3.0
Vcs-Git: https://salsa.debian.org/ruby-team/ruby-batch-loader.git
Vcs-Browser: https://salsa.debian.org/ruby-team/ruby-batch-loader
Homepage: https://github.com/exAspArk/batch-loader
Testsuite: autopkgtest-pkg-ruby
XS-Ruby-Versions: all

Package: ruby-batch-loader
Architecture: all
XB-Ruby-Versions: ${ruby:Versions}
Depends: ruby | ruby-interpreter,
         ruby-graphql,
         ${misc:Depends},
         ${shlibs:Depends}
Description: Powerful tool to avoid N+1 DB or HTTP queries
 This library provides a generic lazy batching mechanism to avoid N+1 DB
 queries, HTTP queries, etc.
 .
 Highlights
  * Generic utility to avoid N+1 DB queries, HTTP requests, etc.
  * Adapted Ruby implementation of battle-tested tools like Haskell Haxl, JS
    DataLoader, etc.
  * Batching is isolated and lazy, load data in batch where and when it's
    needed.
  * Automatically caches previous queries (identity map).
  * Thread-safe (`loader`).
  * No need to share batching through variables or custom defined classes.
  * No dependencies, no monkey-patches, no extra primitives such as Promises.
